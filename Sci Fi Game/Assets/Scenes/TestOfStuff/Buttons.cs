﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Buttons : MonoBehaviour 
{
	public Image Controlls;
	private Color ButtColor;

	private Color ContColor;
	public Image ControlButt;

	private Color QuitColor;
	public Image QuitButt;

	private Color StartColor;
	public Image StartButt;

    public bool Start;
    public bool Controls;
    public bool Quit;

	public void StartButton()
	{
        Start = true;
        Quit = false;
		Application.LoadLevel ("FinalLevel");
	}

	public void ControlButtons()
	{
        Start = false;
        Controls = true;
        Quit = false;

		ButtColor = Controlls.color;
		ButtColor.a = 255;
		Controlls.color = ButtColor;

		StartColor = StartButt.color;
		StartColor.a = 0;
		StartButt.color = StartColor;

		ContColor = ControlButt.color;
		ContColor.a = 0;
		ControlButt.color = ContColor;

		QuitColor = QuitButt.color;
		QuitColor.a = 255;
		QuitButt.color = QuitColor;

	}

	public void QuitButton ()
	{
        Quit = true;
        Controls = false;
        Start = false;

		ButtColor.a = 0;
		Controlls.color = ButtColor;

		StartColor.a = 255;
		StartButt.color = StartColor;

		ContColor.a = 255;
		ControlButt.color = ContColor;

		QuitColor.a = 0;
		QuitButt.color = QuitColor;
	}
}
