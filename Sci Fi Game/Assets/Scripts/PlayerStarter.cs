﻿using UnityEngine;
using System.Collections;

public class PlayerStarter : MonoBehaviour {

	private PlayerController player;

	private bool started = false;

	// Use this for initialization
	void Start () 
	{
		player = GetComponent<PlayerController>();
	}

	void Update( )
	{
		if ( !started )
		{
			print( "working" );

			player.enabled = true;
			started = true;
		}
	}
}
