﻿using UnityEngine;
using System.Collections;

public class CentipedeChase : IState
{
	private IState nextState = null;
	private CentipedeController centipede;
	
	public CentipedeChase( CentipedeController centipede )
	{
		this.centipede = centipede;

		centipede.GetNavAgent().ResetPath( );

        //		Vector3 playerPos = centipede.GetPlayer().transform.position;
        //		playerPos.y = centipede.transform.position.y;
        //		centipede.transform.LookAt( playerPos );

        centipede.centiAnim.SetBool( "Chase", true );
        centipede.chased = true;
    }
	
	public void Update( )
	{
		Vector3 playerPos = centipede.GetPlayer().transform.position;
		playerPos.y = centipede.transform.position.y;

		Vector3 dirToPlayer = ( playerPos - centipede.transform.position ).normalized;

		centipede.moveActuator.LookTowards( dirToPlayer, 4f );
		centipede.moveActuator.MoveInDirection( dirToPlayer, centipede.chaseSpeed );

		if( !centipede.visionSensor.SawThis(centipede.GetPlayer()) )
		{
            centipede.centiAnim.SetBool( "Chase", false );
            centipede.chased = false;
            nextState = new CentipedeWalk( centipede, FindNearestVent() );
		}

		if( centipede.InAttackRange() )
		{
            centipede.centiAnim.SetBool( "Chase", false );
            centipede.chased = false;
            nextState = new CentipedeAttack( centipede );
		}
	}
	
	public IState GetNextState( )
	{
		return nextState;
	}

	CentipedeNode FindNearestVent( )
	{
		int index = 0;
		float dist = Vector3.Distance( centipede.transform.position, CentipedeNode.ventNodes[0].Position() );

		for( int i = 1; i < CentipedeNode.ventNodes.Count; i++ )
		{
			float temp = Vector3.Distance( centipede.transform.position, CentipedeNode.ventNodes[i].Position() );
			if( temp < dist )
			{
				index = i;
			}
		}

		return CentipedeNode.ventNodes[index];
	}
}

















