﻿using UnityEngine;
using System.Collections;

public class CentipedeWalk : IState
{
	private IState nextState = null;
	private CentipedeController centipede;

	public CentipedeWalk( CentipedeController centipede, CentipedeNode startNode )
	{
		this.centipede = centipede;

		centipede.GetNavAgent().ResetPath( );

		centipede.currentNode = startNode;

        centipede.centiAnim.SetBool( "Idle", true );
        centipede.walked = true;
	}

	public void Update( )
	{
		LookAtCurrentPath( );
		Move( );

		bool sawPlayer = centipede.visionSensor.SawThis( centipede.GetPlayer() );
		if ( sawPlayer )
		{
            centipede.centiAnim.SetBool( "Idle", false );
            centipede.walked = false;
            nextState = new CentipedeChase( centipede );
		}

		bool heardPlayer = centipede.noiseSensor.HeardNoiseFrom( centipede.GetPlayer(), centipede.CheckPlayerNoise() );
		if ( heardPlayer )
		{
            centipede.centiAnim.SetBool( "Idle", false );
            centipede.walked = false;
            nextState = new CentipedeAlerted( centipede );
		}

		if ( centipede.InAttackRange() )
		{
            centipede.centiAnim.SetBool( "Idle", false );
            centipede.walked = false;
            nextState = new CentipedeAttack( centipede );
		}
	}

	public IState GetNextState( )
	{
		return nextState;
	}

	void LookAtCurrentPath( )
	{
		Vector3 nodePos = centipede.currentNode.Position( );
		nodePos.y = centipede.transform.position.y;

		Vector3 lookDir = ( nodePos - centipede.transform.position ).normalized;

		centipede.moveActuator.LookTowards( lookDir, 2f );
	}

	void Move( )
	{
		Vector3 nodePos = centipede.currentNode.Position( );
		nodePos.y = centipede.transform.position.y;
		
		Vector3 moveDir = ( nodePos - centipede.transform.position ).normalized;
		centipede.moveActuator.MoveInDirection( moveDir, centipede.walkSpeed );

		if ( Vector3.Distance(centipede.transform.position, centipede.currentNode.Position()) < 0.5f )
		{
			if ( centipede.currentNode.isExitNode )
				centipede.insideVents = false;
			else centipede.insideVents = true;

			int nodeIndex = Random.Range( 0, centipede.currentNode.neighbors.Count );
			centipede.currentNode = centipede.currentNode.neighbors[nodeIndex];
		}
	}
}











