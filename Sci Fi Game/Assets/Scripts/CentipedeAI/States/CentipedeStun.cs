﻿using UnityEngine;
using System.Collections;

public class CentipedeStun : IState
{
	private CentipedeController centipede;
	private IState nextState = null;

	private float timer = 0;

	public CentipedeStun( CentipedeController centipede )
	{
		this.centipede = centipede;
		centipede.stunned = false;

		centipede.moveActuator.StopMoving( );
	}

	public void Update( )
	{
		timer += Time.deltaTime;

		if ( timer >= centipede.stunTime )
		{
			nextState = new CentipedeChase( centipede );
		}
	}

	public IState GetNextState( )
	{
		return nextState;
	}
}
