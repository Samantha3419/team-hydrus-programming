﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CentipedeAlerted : IState
{
	private IState nextState = null;
	private CentipedeController centipede;

	private List<CentipedeNode> exitPath;

	private bool finished = false;

	public CentipedeAlerted( CentipedeController centipede )
	{
		this.centipede = centipede;

		if( centipede.insideVents )
			exitPath = PathFinding.FindPath( centipede.currentNode, FindClosestExitVent() );
		else exitPath = new List<CentipedeNode>( );

        centipede.alerted = true;
    }
	
	public void Update( )
	{
		if( centipede.insideVents )
		{
			LookAtCurrentPath( );
			Move( );
		}
		else
		{
			centipede.moveActuator.StopMoving( );

			if( !centipede.GetNavAgent().hasPath )
			{
				centipede.GetNavAgent().SetDestination( centipede.GetPlayer().transform.position );
			}
			else if( centipede.GetNavAgent().remainingDistance < 1f )
			{
				centipede.GetNavAgent().SetDestination( FindClosestVent().Position() );
				finished = true;
			}
		}

		if( finished && centipede.GetNavAgent().remainingDistance < 0.1f )
		{
            centipede.alerted = false;
            nextState = new CentipedeWalk( centipede, FindClosestVent() );
		}

		if( centipede.visionSensor.SawThis(centipede.GetPlayer()) )
        {
            centipede.alerted = false;
            nextState = new CentipedeChase( centipede );
		}

		if( centipede.InAttackRange() )
        {
            centipede.alerted = false;
            nextState = new CentipedeAttack( centipede );
		}
	}
	
	public IState GetNextState( )
	{
		return nextState;
	}

	CentipedeNode FindClosestVent( )
	{
		int index = 0;
		float dist = Vector3.Distance( centipede.transform.position, CentipedeNode.ventNodes[0].Position() );
		
		for( int i = 1; i < CentipedeNode.ventNodes.Count; i++ )
		{
			float temp = Vector3.Distance( centipede.transform.position, CentipedeNode.ventNodes[i].Position() );
			
			if( temp < dist )
			{
				index = i;
				dist = temp;
			}
		}
		
		return CentipedeNode.ventNodes[index];
	}

	CentipedeNode FindClosestExitVent( )
	{
		int index = 0;
		float dist = Vector3.Distance( centipede.transform.position, CentipedeNode.exitNodes[0].Position() );

		for( int i = 1; i < CentipedeNode.exitNodes.Count; i++ )
		{
			float temp = Vector3.Distance( centipede.transform.position, CentipedeNode.exitNodes[i].Position() );

			if( temp < dist )
			{
				index = i;
				dist = temp;
			}
		}

		return CentipedeNode.exitNodes[index];
	}
	
	void LookAtCurrentPath( )
	{
		Vector3 nodePos = centipede.currentNode.Position( );
		nodePos.y = centipede.transform.position.y;
		
		Vector3 lookDir = ( nodePos - centipede.transform.position ).normalized;
		
		centipede.moveActuator.LookTowards( lookDir, 2f );
	}
	
	void Move( )
	{
		Vector3 nodePos = centipede.currentNode.Position( );
		nodePos.y = centipede.transform.position.y;
		
		Vector3 moveDir = ( nodePos - centipede.transform.position ).normalized;
		centipede.moveActuator.MoveInDirection( moveDir, centipede.walkSpeed );
		
		if( Vector3.Distance(centipede.transform.position, centipede.currentNode.Position()) < 0.5f )
		{
			if( centipede.currentNode.isExitNode )
				centipede.insideVents = false;
			else centipede.insideVents = true;
			
			if( exitPath.Count > 0 )
			{
				centipede.currentNode = exitPath[0];
				exitPath.RemoveAt( 0 );
			}
		}
	}
}

























