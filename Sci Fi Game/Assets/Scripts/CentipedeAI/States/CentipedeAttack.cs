﻿using UnityEngine;
using System.Collections;

public class CentipedeAttack : IState
{
	private IState nextState = null;
	private CentipedeController centipede;
    Vector3 playerPos = Vector3.zero;

    public CentipedeAttack( CentipedeController centipede )
	{
		this.centipede = centipede;

		centipede.moveActuator.StopMoving( );
		centipede.GetNavAgent().ResetPath( );

        playerPos = centipede.GetPlayer().transform.position;
        //playerPos.y = centipede.transform.position.y;

        centipede.attacked = true;
        centipede.centiAnim.SetBool( "Attack", true );
        Debug.Log("Attacking...");
    }
	
	public void Update( )
	{
        Vector3 dirToPlayer = (playerPos - centipede.transform.position).normalized;
        centipede.moveActuator.LookTowards(dirToPlayer, 4f);

        if ( centipede.missed )
        {
            Debug.Log( "MISSED.." );
            centipede.attacked = false;
            bool sawPlayer = centipede.visionSensor.SawThis(centipede.GetPlayer());
            bool heardPlayer = centipede.noiseSensor.HeardNoiseFrom( centipede.GetPlayer(), centipede.CheckPlayerNoise() );
            if( sawPlayer )
            {
                centipede.centiAnim.SetBool( "Attack", false );
                centipede.missed = false;
                nextState = new CentipedeChase( centipede );
            }
		    else if( heardPlayer )
		    {
                centipede.centiAnim.SetBool( "Attack", false );
                centipede.missed = false;
                nextState = new CentipedeAlerted( centipede );
            }
            else
            {
                centipede.centiAnim.SetBool( "Attack", false );
                centipede.missed = false;
                nextState = new CentipedeWalk( centipede, FindClosestVent() );
            }
        }
	}
	
	public IState GetNextState( )
	{
		return nextState;
	}

    CentipedeNode FindClosestVent()
    {
        int index = 0;
        float dist = Vector3.Distance(centipede.transform.position, CentipedeNode.ventNodes[0].Position());

        for( int i = 1; i < CentipedeNode.ventNodes.Count; i++ )
        {
            float temp = Vector3.Distance(centipede.transform.position, CentipedeNode.ventNodes[i].Position());

            if( temp < dist )
            {
                index = i;
                dist = temp;
            }
        }

        return CentipedeNode.ventNodes[index];
    }
}