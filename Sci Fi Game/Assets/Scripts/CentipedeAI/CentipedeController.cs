﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CentipedeController : MonoBehaviour 
{
	private IState state = null;
	private GameObject player;

	private NavMeshAgent navAgent;

	public MoveActuator moveActuator;
	public VisionSensor visionSensor;
	public NoiseSensor noiseSensor;

	public List<CentipedeNode> patrolPath = new List<CentipedeNode>( );
	public CentipedeNode currentNode = null;

	[Header( "Hearing Info" )]
	public float hearingRange = 4f;

	[Header( "Vision Info" )]
	public float visionRange = 5f;
	public float FOV = 90f;
    public LayerMask layerMask;

    [Header( "State Info" )]
	public float walkSpeed = 2f;
	public float chaseSpeed = 5f;
	public float attackRange = 2f;
	public float stunTime = 1f;
	public bool insideVents = true;

    public Animator centiAnim;
    public bool attacked = false;
    public bool missed = false;
    public bool alerted = false;
    public bool walked = false;
    public bool chased = false;
	public bool stunned = false;

	void Awake( )
	{
		navAgent = GetComponent<NavMeshAgent>( );

		PathFinding.Init( );

		player = GameObject.FindGameObjectWithTag( "Player" );

		moveActuator = new MoveActuator( this.gameObject );
		noiseSensor = new NoiseSensor( this.gameObject, hearingRange );
		visionSensor = new VisionSensor( this.gameObject, visionRange, FOV );
        visionSensor.SetLayerMask( layerMask );

		FindPathNodes( );
		SetStartNode( );

		state = new CentipedeWalk( this, currentNode );
	}

	void Update( )
	{
		state.Update( );

		IState nextState = state.GetNextState( );
		if( stunned )
			nextState = new CentipedeStun( this );

		if( nextState != null )
		{
			state = nextState;
			print( state );
		}
	}

	public void SetStateToStunned( )
	{
		stunned = true;
	}
	
	public GameObject GetPlayer( )
	{
		return player;
	}
	
	public NavMeshAgent GetNavAgent( )
	{
		return navAgent;
	}
	
	public bool InAttackRange( )
	{
		if ( Vector3.Distance(transform.position, player.transform.position) <= attackRange )
			return true;
		
		return false;
	}
	
	void OnDrawGizmos( )
	{
		Gizmos.color = Color.magenta;
		Gizmos.DrawWireSphere( transform.position, hearingRange );
	}
	
	public float CheckPlayerNoise( )
	{
		return player.GetComponent<PlayerController>().NoiseRadius( );
	}

	void FindPathNodes( )
	{
		GameObject[] nodes = GameObject.FindGameObjectsWithTag( "CentipedeNodes" );

		for ( int i = 0; i < nodes.Length; i++ )
		{
			CentipedeNode centiNode = nodes[i].GetComponent<CentipedeNode>( );
			patrolPath.Add( centiNode );
		}
	}

	void SetStartNode( )
	{
		GameObject[] nodes = GameObject.FindGameObjectsWithTag( "CentipedeNodes" );

		if ( nodes.Length == 0 )
		{
			print( "Error: No nodes for the centipede to traverse." );
			return;
		}

		int index = 0;
		float dist = Vector3.Distance( transform.position, nodes[0].transform.position );
		
		for( int i = 1; i < nodes.Length; i++ )
		{
			float temp = Vector3.Distance( transform.position, nodes[i].transform.position );
			if ( temp < dist )
			{
				index = i;
				dist = temp;
			}
		}

		currentNode = nodes[index].GetComponent<CentipedeNode>( );
	}

    public void CentipedeRestart( )
    {
        SetStartNode( );
        IState nextState = state.GetNextState( );
        nextState = new CentipedeWalk( this, currentNode );

        attacked = false;
        alerted = false;
        chased = false;
        walked = false;
        missed = false;
        
        centiAnim.SetBool( "Idle", false );
        centiAnim.SetBool( "Chase", false );
        centiAnim.SetBool( "Attack", false );
    }

    void Attack( )
    {
        Debug.DrawRay( transform.position, transform.forward * 5, Color.blue );
        RaycastHit hit;
        if( Physics.Raycast(transform.position, transform.forward * 5, out hit, 5.0f) )
        {
            PlayerController player = hit.collider.gameObject.GetComponent<PlayerController>( );
            Debug.Log( player );
            if( player != null )
                attacked = true;
        }
    }

    void Missed( )
    {
        missed = true;
    }
}






































