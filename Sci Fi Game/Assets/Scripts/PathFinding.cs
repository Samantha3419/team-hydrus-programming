﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathFinding 
{
	static private List<CentipedeNode> nodes = new List<CentipedeNode>( );

	static public void Init( )
	{
		GameObject[] allNodes = GameObject.FindGameObjectsWithTag( "CentipedeNodes" );
		for ( int i = 0; i < allNodes.Length; i++ )
		{
			nodes.Add( allNodes[i].GetComponent<CentipedeNode>() );
		}
	}

	static public List<CentipedeNode> FindPath( CentipedeNode start, CentipedeNode goal )
	{
		CentipedeNode current = null;

		List<CentipedeNode> closedList = new List<CentipedeNode>( );
		List<CentipedeNode> openList = new List<CentipedeNode>( );

		openList.Add( start );

		while ( openList.Count > 0 )
		{
			current = openList[0];
			for ( int i = 0; i < openList.Count; i++ )
			{
				if ( openList[i].fCost < current.fCost || openList[i].fCost == current.fCost && openList[i].hCost < current.hCost )
					current = openList[i];
			}

			openList.Remove( current );
			closedList.Add( current );

			if ( current == goal )
			{
				return RetracePath( start, goal );
			}

			foreach ( CentipedeNode neighbor in current.neighbors )
			{
				if ( closedList.Contains(neighbor) )
					continue;

				float moveCostToNeighbor = current.gCost + GetDistance(current, neighbor);
				if ( moveCostToNeighbor < neighbor.gCost || !openList.Contains(neighbor) )
				{
					neighbor.gCost = moveCostToNeighbor;
					neighbor.hCost = GetDistance(neighbor, goal);
					neighbor.parent = current;

					if ( !openList.Contains(neighbor) )
					{
						openList.Add( neighbor );
					}
				}
			}
		}

		return new List<CentipedeNode>( );
	}

	static List<CentipedeNode> RetracePath( CentipedeNode start, CentipedeNode end )
	{
		List<CentipedeNode> path = new List<CentipedeNode>( );
		CentipedeNode current = end;

		while ( current != start )
		{
			path.Add( current );
			current = current.parent;
		}

		path.Reverse( );

		return path;
	}

	static float GetDistance( CentipedeNode lhs, CentipedeNode rhs )
	{
		return Vector3.Distance( lhs.Position(), rhs.Position() );
	}
}

























