﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class RestartLevel : MonoBehaviour
{
    public PlayerController player;
    public SpiderController spider;
    public CentipedeController centipede;
    private GameObject artifact;

    void Awake( )
    {
		artifact = GameObject.FindGameObjectWithTag( "Artifact" );
    }

    void Update( )
    {
		if( spider != null && spider.attacked )
            Restart( );
        if( centipede != null && centipede.attacked )
            Restart( );
    }

    void OnGUI( )
    {
        GUILayout.Button( "L-Shift::Run" );
        GUILayout.Button( "Space::Crouch" );
        GUILayout.Button( "E::Use Hiding Spot" );
        if( GUILayout.Button( "Reload Level" ) )
        {
            Restart( );
        }
    }

    void Restart( )
    {
        if( player != null )
        {
            player.PlayerRestart( );
            artifact.GetComponent<ArtifactScript>().Reset( );
        }

        if( spider != null )
        {
            spider.SpiderRestart();
        }
        if( centipede != null )
        {
            centipede.CentipedeRestart();
        }
    }
}
