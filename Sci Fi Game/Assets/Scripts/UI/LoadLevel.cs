﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour {

    public bool menu;
    public bool controls;
	public GameObject mainMenu;
	public GameObject mainMenuBackground;
	public GameObject controlsMenu;
	public GameObject[] objects;
	public GameObject canvas;


	public void LoadMainMenu( )
    {
        menu = true;
        controls = false;
		mainMenuBackground.SetActive( true );
		controlsMenu.SetActive( false );
        for( int i=0; i < objects.Length; ++i )
        {
            objects[i].SetActive( false );
        }

	}
	
	public void LoadControls( )
    {
        controls = true;
        menu = false;
		controlsMenu.SetActive( true );
		mainMenuBackground.SetActive( false );
	}
	
	public void LoadGame( )
    {
        menu = false;
        controls = false;
		canvas.SetActive (false);
        for( int i=0; i < objects.Length; ++i )
        {
            objects[i].SetActive( true );
        }

		mainMenu.SetActive (false);
		controlsMenu.SetActive (false);
	}
}
