﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NoiseSensorGUI : MonoBehaviour 
{
	private Image noiseMeter;
	private PlayerController player;

	private Rect defaultImageRect;

	void Awake( )
	{
		noiseMeter = GetComponent<Image>( );
		defaultImageRect = noiseMeter.rectTransform.rect;

		GameObject go = GameObject.FindGameObjectWithTag( "Player" );
		player = go.GetComponent<PlayerController>( );
	}

	void Update( )
	{
		float ratio = player.NoiseRadius() / player.runSpeed;

		noiseMeter.rectTransform.sizeDelta = new Vector2( defaultImageRect.width * ratio, defaultImageRect.height );
	}
}
