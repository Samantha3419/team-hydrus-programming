﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmmoGUI : MonoBehaviour 
{
	public Text currentAmmo;
	public Text maxAmmo;

	private PlayerController player;

	void Awake( )
	{
		GameObject p = GameObject.FindGameObjectWithTag( "Player" );
		player = p.GetComponent<PlayerController>( );

		player.gun.OnShoot += UpdateAmmo;

		maxAmmo.text = player.gun.ammo.ToString( );
	}

	void UpdateAmmo( int ammoCount )
	{
		currentAmmo.text = ammoCount.ToString( );
	}

	void OnDestroy( )
	{
		player.gun.OnShoot -= UpdateAmmo;
	}
}
