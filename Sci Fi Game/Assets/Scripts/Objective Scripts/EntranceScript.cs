﻿using UnityEngine;
using System.Collections;

public class EntranceScript : MonoBehaviour 
{
	public bool artifactEquip;
	private Animator anim;
	public bool ending = false;

	private GameObject player;
	public Transform endLoc;
	public float speed;

	PlayerController playCon;
	private Camera cam;

	void Awake ()
	{
		artifactEquip = false;
		anim = gameObject.GetComponent<Animator> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		cam = Camera.main;
	}

	void FixedUpdate()
	{
		if(ending == true)
		{
			float t = 0f;
			if (t < speed)
			{
				t +=Time.deltaTime;
				float fract = t / speed;
				player.transform.position = Vector3.Lerp(player.transform.position, endLoc.position, fract);
				player.transform.rotation = Quaternion.RotateTowards(player.transform.rotation, endLoc.rotation, fract);
				cam.transform.rotation = Quaternion.Lerp(cam.transform.rotation, Quaternion.identity, fract);
			}
		}
	}

	void Equipped()
	{
		artifactEquip = true;
	}

	void UnEquipped()
	{
		artifactEquip = false;
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Player")
		{
			if(artifactEquip)
			{
				anim.SetTrigger("Open");
				PositionPlayer();
			}
		}
	}

	void PositionPlayer()
	{
		player.GetComponent<PlayerController> ().enabled = false;
		ending = true;
	}

	IEnumerator Win()
	{
		yield return new WaitForSeconds (0.0f);
		//win screen fade in
	}
}
