﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ArtifactScript : MonoBehaviour 
{
	public GameObject partEffect;
	public Transform node;
	public float waitTime;
	public float appearTime;
	public float offset;
	public bool pickedUp;
	private GameObject player;
	private Renderer showing;

	public GameObject entranceDoor;

	public Transform cam;
	public LayerMask myLayerMask;

	public Material[] materials;

	public Text text;

	public Transform startPos;
	
	void Awake()
	{
		cam = Camera.main.transform;
		player = GameObject.FindGameObjectWithTag ("Player");
		pickedUp = false;
		showing = gameObject.GetComponentInChildren<Renderer>();
		text.enabled = false;
	}

	// Update is called once per frame
	void Update () 
	{
		RaycastHit hit;

		if(!pickedUp)
		{
			if(Physics.Raycast (cam.position, cam.forward, out hit, 3, myLayerMask))
			{

				if (hit.collider.tag == "Artifact")
				{
					Debug.Log ("hit");
					text.enabled = true;
					gameObject.GetComponentInChildren<Renderer> ().material = materials [1];

					if(Input.GetKeyDown(KeyCode.Y))
					{							
						pickedUp = true;
						StartCoroutine("Teleport");
					}
				}
			}
			else
				DefaultMat();
				Debug.Log ("not hit");
		}
		else
			DefaultMat ();

		Debug.DrawRay (cam.position, cam.forward * 3, Color.green);

	}

	void DefaultMat()
	{
		text.enabled = false;
		gameObject.GetComponentInChildren<Renderer> ().material = materials [0];
	}
	
	IEnumerator Teleport()
	{
		//Artifact Dissappears
		Instantiate (partEffect, transform.position, transform.rotation);
		StartCoroutine ("Appearance");
		yield return new WaitForSeconds (waitTime);

		//Artifact Reappears in front of player
		transform.parent = player.transform;
		transform.localPosition = node.localPosition;
		transform.LookAt (new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z));
		GameObject p_Effect2 = Instantiate (partEffect, transform.position, transform.rotation) as GameObject;
		p_Effect2.transform.parent = player.transform;
		StartCoroutine ("Appearance");
		yield return new WaitForSeconds (appearTime);

		//Artifact Dissappears again
		GameObject p_Effect3 = Instantiate (partEffect, transform.position, transform.rotation) as GameObject;
		p_Effect3.transform.parent = player.transform;
		StartCoroutine ("Appearance");

		entranceDoor.SendMessage ("Equipped");
	}

	IEnumerator Appearance()
	{
		yield return new WaitForSeconds (0.25f);
		showing.enabled = !showing.enabled;

	}

	public void Reset()
	{
		print (startPos.position);
		entranceDoor.SendMessage ("UnEquipped");
		transform.parent = null;
		transform.position = startPos.position;
		transform.rotation = startPos.rotation;
		showing.enabled = true;
		pickedUp = false;
	}
}
