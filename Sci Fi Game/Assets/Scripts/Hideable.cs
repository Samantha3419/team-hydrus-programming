﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hideable : MonoBehaviour
{
	public List<Vector3> exitSpots = new List<Vector3>( );

	public Transform hidingSpot;

	/*-------------------*/

	public void Hide( GameObject camera )
	{
		camera.transform.parent = null;
		camera.transform.position = hidingSpot.position;
	}

	public Vector3 Exit( Transform camera )
	{
		float exitHere = 0.25f;
		Vector3 exitSpotPos = camera.position;

		for (int i = 0; i < exitSpots.Count; i++) 
		{
			Vector3 dirToExit = (exitSpots[i] - camera.position).normalized;
			float temp = Vector3.Dot (dirToExit, camera.forward);
			
			if (temp >= exitHere) 
			{
				exitHere = temp;
				exitSpotPos = exitSpots [i];
			}
		}

		return exitSpotPos;
	}
}
