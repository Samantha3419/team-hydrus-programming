﻿using UnityEngine;
using System.Collections;

public class Crouch : MonoBehaviour 
{
	public bool crouching;
	public Camera mainCamera;

	// Update is called once per frame
	void Update () 
	{
		if(crouching)
		{
			mainCamera.transform.localPosition = new Vector3 (0, 0.1f, 0);
		} 
		else 
		{
			mainCamera.transform.localPosition = new Vector3 (0, 0.8f, 0);
		}

		if (Input.GetKeyDown (KeyCode.LeftControl)) 
		{
			if (crouching) {
				StopCrouching ();
				return;
			}
			if (!crouching)
				Crouching ();
		}
	}

	void Crouching() 
	{
		this.GetComponent<CharacterController>().height = 1f;
		this.GetComponent<CharacterController>().center = new Vector3(0f,-0.4f,0f);
		crouching = true;
	}

	void StopCrouching(){
		crouching = false;
		this.GetComponent<CharacterController>().height = 1.8f;
		this.GetComponent<CharacterController>().center = new Vector3(0f,0f,0f);
	}
}
