﻿using UnityEngine;
using System.Collections;

public class PlayerController_Emon : MonoBehaviour
{
    private CharacterController m_CharacterController;
    private Transform m_Transform;
    private Vector3 m_Velocity = Vector3.zero;
	[Header( "Player Movement Info" )]
	[SerializeField]
	private float m_WalkSpeed;
	[SerializeField]
	private float m_RunSpeed;
	[SerializeField]
    private float m_CrouchSpeed;

    private Camera m_Camera;
	private float m_HeadTilt;
	[Header( "Player Camera Info" )]
	[SerializeField]
	private Vector2 yRotationAmount = Vector2.zero;
	[SerializeField]
	private float m_RotationSpeed;
	[SerializeField]
	private float hidingCheckRange = 5;

	//Ronaele Added this
	private GameObject flashlight;
	public bool flashON = false;

	public bool isHiding = false;
	Hideable hidingSpot;

	[SerializeField] private bool exit;
    void Start ()
    {
        m_CharacterController = gameObject.GetComponent<CharacterController> ();
        m_Transform = transform;
        m_Camera = Camera.main;

        m_HeadTilt = m_Camera.transform.localEulerAngles.x;

		//Ronaele Added this
		flashlight = GameObject.FindGameObjectWithTag("Flashlight");
    }

	void Update ()
    {
        CameraRotate();
        CharacterMove ();
		if ( Input.GetKeyDown("e") && !isHiding )
        	CharacterInteract ();
		else if ( Input.GetKeyDown("e") && isHiding )
			ExitHidingSpot( );
		//Ronaele Added this
		if(Input.GetKeyDown("f") && !flashON)
		{
			flashlight.gameObject.SetActive(true);
			flashON = true;
		}
		else if (Input.GetKeyDown("f") && flashON)
		{
			flashlight.gameObject.SetActive(false);
			flashON = false;
		}

	}

    void CameraRotate ()
    {
        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");

        float xR = x * m_RotationSpeed * Time.deltaTime;
        m_Transform.Rotate(Vector3.up, xR);

        float yR = y * m_RotationSpeed * Time.deltaTime * -1;
        m_HeadTilt += yR;
        m_HeadTilt = Mathf.Clamp(m_HeadTilt, yRotationAmount.x, yRotationAmount.y);

        Vector3 newRotation = new Vector3(m_HeadTilt,
                                          m_Camera.transform.localEulerAngles.y,
                                          m_Camera.transform.localEulerAngles.z);

        m_Camera.transform.localEulerAngles = newRotation;
    }

    void CharacterMove ()
    {
        float h = Input.GetAxis ("Horizontal");
        float v = Input.GetAxis ("Vertical");
        bool run = Input.GetButton ("Run");
        bool crouch = Input.GetButton ("Crouch");

        m_Velocity = new Vector3 (h, 0, v);
        m_Velocity = m_Transform.TransformDirection (m_Velocity);
        m_Velocity = m_Velocity.normalized;

        if (!run && !crouch)
            m_Velocity *= m_WalkSpeed;
        else if (run && !crouch)
            m_Velocity *= m_RunSpeed;

        if (crouch)
        {
            //Make player crouch
            m_Velocity *= m_CrouchSpeed;
        }

        m_CharacterController.Move (m_Velocity * Time.deltaTime);
    }

    void CharacterInteract ()
    {
		RaycastHit hit;
		Debug.DrawRay(m_Camera.transform.position, m_Camera.transform.forward * hidingCheckRange, Color.yellow);
		if ( Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, hidingCheckRange) )
		{
			hidingSpot = hit.collider.gameObject.GetComponent<Hideable>();

			if ( hidingSpot != null )
			{
				hidingSpot.Hide( this.gameObject );
				isHiding = true;
			}
		}
    }

	void ExitHidingSpot( )
	{
		transform.position = hidingSpot.Exit( this.transform );

		isHiding = false;
		hidingSpot = null;
	}







   








}
