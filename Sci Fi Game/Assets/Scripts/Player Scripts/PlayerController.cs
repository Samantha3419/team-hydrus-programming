﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
    private PlayerState state;

	private CharacterController controller = null;
	private Camera playerCam;
	private Vector3 playerCamPos;
	private Hideable hideSpot = null;

	public Gun gun;
	public GameObject HUDPrefab;
	public GameObject camHolder;
    public GameObject PlayerRestartPosition;

	public Vector3 velocity { get; set; }

	[Header( "Movement" )]
	public float accelration = 1f;
	public float crouchSpeed = 2.5f;
	public float walkSpeed = 5f;
	public float runSpeed = 10f;
	public bool walking = false;
	public bool run = false;
	[Range( 0, 1 )]
	public float crouchHeight = 1;
	public float defaultCrouchHeight { get; set; }
	private float currentSpeed = 0;

	public Vector2 lookAxis { get; set; }
	public Vector2 moveAxis { get; set; }
	public bool useKey { get; set; }
	public bool runKey { get; set; }
	public bool crouchKey { get; set; }

	[Header( "Controller Sensitivity" )]
	public Vector2 rotationSpeed = new Vector2( 200f, 200f );
	public Vector2 upwardLookClamp = new Vector2( -45, 45 );
	public bool crouchOnHold = true;

	[Header( "Debugging" )]
	public string nameOfState = "";

	//Ronaele Added this
	public GameObject flashlight;
	public bool flashON = false;
    
	void Awake( )
	{
		controller = GetComponent<CharacterController>( );
		playerCam = GetComponentInChildren<Camera>( );
		playerCamPos = camHolder.transform.localPosition;

		GameObject hud = GameObject.FindGameObjectWithTag( "HUD" );
		if ( hud == null )
		{
			hud = GameObject.Instantiate( HUDPrefab ) as GameObject;
			hud.name = "HUD";
		}

		state = new Player_Idle( this );

		defaultCrouchHeight = controller.height;

        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = false;

		//Ronaele Added this
		flashlight = GameObject.FindGameObjectWithTag("Flashlight");
		flashlight.SetActive(false);

	}

	void Update( )
	{
        //if ( Cursor.lockState != CursorLockMode.Locked )
        //    Cursor.visible = true;

		HandleInput( );

		state.Update( );

		nameOfState = state.GetType().Name;

		if ( !IsHiding() )
			Shoot( );

		PlayerState nextState = state.GetNextState( );
		if ( nextState != null )
		{
			state = nextState;
		}

		//Ronaele Added this
		if(Input.GetKeyDown("f") && !flashON)
		{
			flashlight.SetActive(true);
			flashON = true;
		}
		else if (Input.GetKeyDown("f") && flashON)
		{
			flashlight.SetActive(false);
			flashON = false;
		}
	}

	public void Shoot( )
	{
		if ( Input.GetMouseButtonDown(0) )
			gun.Shoot( gun.transform.forward );
	}

	void HandleInput( )
	{
		// Move and look axis input
		moveAxis = new Vector2( Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical") );
		lookAxis = new Vector2( Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y") );

		// Use button input
		if ( Input.GetKeyDown(KeyCode.E) )
			useKey = true;
		else useKey = false;

        if ( Input.GetKey(KeyCode.Space) )
            crouchKey = true;
        else crouchKey = false;

        if ( Input.GetKey(KeyCode.LeftShift) )
            runKey = true;
        else runKey = false;
    }

	public void Move( float speed )
	{
		currentSpeed += accelration;
		if ( currentSpeed >= speed )
			currentSpeed = speed;
		currentSpeed = ( speed == 0 ) ? 0 : currentSpeed;

		Vector3 dir = transform.right * moveAxis.x + transform.forward * moveAxis.y;
		dir.Normalize( );

		velocity = dir * currentSpeed;
		velocity = new Vector3( velocity.x, velocity.y - 10, velocity.z );

		controller.Move( velocity * Time.deltaTime );
	}

	private float headTilt = 0;
	public void RotateCamera( )
	{
		float xR = lookAxis.x * rotationSpeed.x * Time.deltaTime;
		transform.Rotate( transform.up, xR);

		if ( camHolder.transform.parent == null )
			camHolder.transform.Rotate( transform.up, xR );
		
		float yR = lookAxis.y * rotationSpeed.y * Time.deltaTime * -1;
		headTilt += yR;
		headTilt = Mathf.Clamp( headTilt, upwardLookClamp.x, upwardLookClamp.y );

		Vector3 newRotation = new Vector3( headTilt,
				                           playerCam.transform.localEulerAngles.y,
				                           playerCam.transform.localEulerAngles.z );

		playerCam.transform.localEulerAngles = newRotation;
	}

	public void Hide( )
	{
		RaycastHit hit;
		float useRange = 3f;

		Debug.DrawRay( playerCam.transform.position, playerCam.transform.forward* useRange, Color.magenta );
		if ( Physics.Raycast(playerCam.transform.position, playerCam.transform.forward, out hit, useRange) )
		{
			hideSpot = hit.transform.GetComponent<Hideable>( );
			if ( hideSpot != null )
			{
				hideSpot.Hide( camHolder );
			}
		}
	}
	
	public void ExitHide( )
	{
		Vector3 exitPos = hideSpot.Exit( camHolder.transform );
		
		if ( exitPos == camHolder.transform.position )
			return;
		
		camHolder.transform.parent = this.transform;
		camHolder.transform.localPosition = playerCamPos;

		transform.position = exitPos;
		hideSpot = null;
	}

	public void ResetCrouchHeight( )
	{
		controller.height = defaultCrouchHeight;
	}

	public bool IsHiding( )
	{
		return ( hideSpot != null ) ? true : false;
	}

	public CharacterController GetCharController( )
	{
		return controller;
	}

	public float NoiseRadius( )
	{
		if ( controller == null )
			return 0;

		return controller.velocity.magnitude;
	}

	void OnDrawGizmos( )
	{
		//Noise Radius debugging
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere( transform.position, NoiseRadius() );
	}

    public void PlayerRestart( )
    {
		transform.position = PlayerRestartPosition.transform.position;
    }

	public Vector3 HidingPosition( )
	{
		return playerCam.transform.position;
	}
}





























