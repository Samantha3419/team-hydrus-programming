﻿using UnityEngine;
using System.Collections;

public class Player_Run : PlayerState 
{
	public Player_Run( PlayerController player ) : base( player )
	{
	}
	
	public override void Update( )
	{
		player.run = true;

		if ( SwitchState() )
			return;

		player.RotateCamera( );
		
		if ( player.useKey )
		{
			player.Hide( );
		}

		player.Move( player.runSpeed );
	}
	
	public override bool SwitchState( )
	{
		if ( player.moveAxis == Vector2.zero )
		{
			player.run = false;
			nextState = new Player_Idle( player );
		}
		else if ( !player.runKey )
		{
			player.run = false;
			nextState = new Player_Walk( player );
		}
		
		if ( player.crouchKey )
		{
			player.run = false;
			nextState = new Player_Crouch( player );
		}		
		
		if ( player.IsHiding() )
		{
			player.run = false;
			nextState = new Player_Hide( player );
		}
		
		return ( nextState != null ) ? true : false;
	}
}