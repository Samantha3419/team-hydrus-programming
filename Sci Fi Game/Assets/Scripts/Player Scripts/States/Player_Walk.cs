﻿using UnityEngine;
using System.Collections;

public class Player_Walk : PlayerState 
{
	public Player_Walk( PlayerController player ) : base( player )
	{
	}
	
	public override void Update( )
	{
		player.walking = true;

		if ( SwitchState() )
			return;

		player.RotateCamera( );
		
		if ( player.useKey )
		{
			player.Hide( );
		}

		player.Move( player.walkSpeed );
	}
	
	public override bool SwitchState( )
	{
		if ( player.moveAxis == Vector2.zero )
		{
			player.walking = false;
			nextState = new Player_Idle( player );
		}
		else if ( player.runKey )
		{
			player.walking = false;
			nextState = new Player_Run( player );
		}

		if ( player.crouchKey )
		{
			player.walking = false;
			nextState = new Player_Crouch( player );
		}
		
		if ( player.IsHiding() )
		{
			player.walking = false;
			nextState = new Player_Hide( player );
		}
		
		return ( nextState != null ) ? true : false;
	}
}