﻿using UnityEngine;
using System.Collections;

public class Player_Idle : PlayerState 
{
	public Player_Idle( PlayerController player ) : base( player )
	{
	}

	public override void Update( )
	{
		if ( SwitchState() )
			return;

		player.RotateCamera( );

		if ( player.useKey )
		{
			player.Hide( );
		}

		player.Move( 0 );
	}

	public override bool SwitchState( )
	{
		if ( player.moveAxis != Vector2.zero )
		{
			nextState = new Player_Walk( player );
		}

		if ( player.crouchKey )
		{
			nextState = new Player_Crouch( player );
		}

		if ( player.IsHiding() )
		{
			nextState = new Player_Hide( player );
		}

		return ( nextState != null ) ? true : false;
	}
}
