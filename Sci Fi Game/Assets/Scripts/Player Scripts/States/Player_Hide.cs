﻿using UnityEngine;
using System.Collections;

public class Player_Hide : PlayerState 
{
	public Player_Hide( PlayerController player ) : base( player )
	{
		player.ResetCrouchHeight( );
	}
	
	public override void Update( )
	{
		if ( SwitchState() )
			return;

		player.RotateCamera( );

		if ( player.useKey )
		{
			player.ExitHide( );
		}

		player.Move( 0 );
	}
	
	public override bool SwitchState( )
	{
		if ( !player.IsHiding() )
		{
			nextState = new Player_Idle( player );
		}
		
		return ( nextState != null ) ? true : false;
	}
}