﻿using UnityEngine;
using System.Collections;

public abstract class PlayerState 
{
	protected PlayerController player;

	protected PlayerState nextState = null;

	public abstract void Update( );
	public abstract bool SwitchState( );

	public PlayerState( PlayerController player )
	{
		this.player = player;
	}

	public PlayerState GetNextState( )
	{
		return nextState;
	}
}
