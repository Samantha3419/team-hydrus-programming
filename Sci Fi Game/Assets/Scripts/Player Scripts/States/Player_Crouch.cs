﻿using UnityEngine;
using System.Collections;

public class Player_Crouch : PlayerState 
{
	public Player_Crouch( PlayerController player ) : base( player )
	{
		CharacterController controller = player.GetCharController( );
		
		controller.height = player.crouchHeight + 1;
	}
	
	public override void Update( )
	{
		if ( SwitchState() )
			return;
		
		player.RotateCamera( );
		
		if ( player.useKey )
		{
			player.Hide( );
		}

		player.Move( player.crouchSpeed );
	}
	
	public override bool SwitchState( )
	{
		if ( !player.crouchKey )
		{
			nextState = new Player_Idle( player );
			player.ResetCrouchHeight( );
		}
		
		if ( player.IsHiding() )
		{
			nextState = new Player_Hide( player );
		}
		
		return ( nextState != null ) ? true : false;
	}

}