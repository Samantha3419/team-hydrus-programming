﻿using UnityEngine;
using System.Collections;

public class OpenTrigger : MonoBehaviour
{

    [SerializeField] private Transform targetR;
    [SerializeField] private Transform targetL;
    [SerializeField] private SlidingDoor door;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Spider")
        {
            door.Open(targetR, targetL);
            door.open = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        door.open = false;
    }
    
}
