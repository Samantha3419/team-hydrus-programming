﻿using UnityEngine;
using System.Collections;

public class SlidingDoor : MonoBehaviour 
{
    public Transform closeTargetR;
    public Transform openTargetR;
    public Transform closeTargetL;
    public Transform openTargetL;

	[SerializeField] private GameObject doorR;
    [SerializeField] private GameObject doorL;

	[SerializeField] private float openTime = 3;
	[SerializeField] private float speed = 30;
	[SerializeField] private float timeOpened;
	public bool open = false;
    [SerializeField] private float timer;

	public void Open(Transform targetR, Transform targetL)
	{
		if(!open)
		{
			openTargetR = targetR;
            openTargetL = targetL;
			open = true;
			timeOpened = Time.time;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (open == true)
        {
            timer += Time.deltaTime * speed;
            if(timer > 1)
            {
                timer = 1;
            }
            doorR.transform.position = Vector3.Lerp(closeTargetR.transform.position, openTargetR.transform.position, timer);
            doorL.transform.position = Vector3.Lerp(closeTargetL.transform.position, openTargetL.transform.position, timer);
        }
        if (open == false)
        {
            timer -= Time.deltaTime * speed;
            if (timer < 0)
            {
                timer = 0;
            }
            doorR.transform.position = Vector3.Lerp(closeTargetR.transform.position, openTargetR.transform.position, timer);
            doorL.transform.position = Vector3.Lerp(closeTargetL.transform.position, openTargetL.transform.position, timer);
        }
        /*
        if (openTargetR != null && openTargetL != null)
        {
            doorR.transform.position = Vector3.Lerp(doorR.transform.position, openTargetR.transform.position, timer);
            doorL.transform.position = Vector3.Lerp(doorL.transform.position, openTargetL.transform.position, timer);
        }
        if (Time.time - timeOpened > openTime)
        {
            openTargetR = closeTargetR;
            openTargetL = closeTargetL;
        }
         */ 
        /*
		if((Time.time - timeOpened)*2 > openTime )
		{
			open = false;
		}
         */
        //Debug.Log(timer);
	}
}
