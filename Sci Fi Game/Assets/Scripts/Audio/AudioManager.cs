﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour {

    [Header( "GameObjects" )]
    public PlayerController player;
    public SpiderController spider;
    public CentipedeController centipede;

    [Header( "Main BackGroung Music" )]
    [Header( "Intro Song" )]
    public AudioMixerSnapshot Intro;
    public AudioSource IntroSource;
    [Header( "Controls Song" )]
    public AudioMixerSnapshot Controls;
    public AudioSource ControlsSource;
    [Header( "Load Song" )]
    public AudioMixerSnapshot Load;
    public AudioSource LoadSource;
    [Header( "Calm Audio" )]
	public AudioMixerSnapshot Calm;
    public AudioSource CalmSource;
    [Header( "CHASED Audio" )]
	public AudioMixerSnapshot Chase;
    public AudioSource ChaseSource;
    [Header( "Get Key Audio" )]
    public AudioMixerSnapshot GetKey;
    public AudioSource KeySource;
    [Header( "WIN Audio" )]
    public AudioMixerSnapshot WIN;
    public AudioSource WINSource;

    public GameObject artifact;
    public GameObject ending;
    public Buttons buttons;

    int introCounter = 0;
    int controlCounter = 0;
    int loadCounter = 0;
    bool loaded = false;

    int chasedCounter = 0;
    int calmCounter = 0;
    int winCounter = 0;
    int getKeyCounter = 0;

    void Awake( )
    {
        DontDestroyOnLoad( this.gameObject );
    }

    void Update( )
    {
        if( buttons.Start == false )
        {
            if( buttons.Controls == false )
            {
                controlCounter = 0;
                if( introCounter == 0 )
                {
                    ControlsSource.Stop( );
                    Intro.TransitionTo( 0 );
                    IntroSource.Play( );
                    introCounter = 1;
                }
            }
            else
            {
                introCounter = 0;
                if( controlCounter == 0 )
                {
                    IntroSource.Stop( );
                    Controls.TransitionTo( 0 );
                    ControlsSource.Play( );
                    controlCounter = 1;
                }
                if( buttons.Quit == true  )
                {
                    buttons.Controls = false;
                }
            }
        }
        else
        {
            loaded = true;
            buttons.enabled = false;
        }
        
        if( loaded == true )
        {
            if( ending.GetComponent<EntranceScript>().ending == false )
            {
                winCounter = 0;
                if( artifact.GetComponent<ArtifactScript>().pickedUp == false )
                {
                    getKeyCounter = 0;
                    if( spider.chased == true )
                    {
                        calmCounter = 0;
                        if( chasedCounter == 0 )
                        {
                            CalmSource.Stop( );
                            Chase.TransitionTo( 2f );
                            ChaseSource.Play( );
                            chasedCounter = 1;
                        }
                    }
		            else
                    {
                        chasedCounter = 1;
                        if( calmCounter == 0 && CalmSource.isPlaying == false )
                        {
                            ChaseSource.Stop( );
                            Calm.TransitionTo( 0.75f * 5f );
                            CalmSource.Play( );
                            calmCounter = 1;
                        }
                    }
                }
                else
                {
                    if( getKeyCounter == 0 )
                    {
                        GetKey.TransitionTo( 0f );
                        KeySource.Play( );
                        getKeyCounter = 1;
                    }
                }
            }
            else
            {
                if( winCounter == 0 )
                {
                    WIN.TransitionTo( 4f );
                    WINSource.Play( );
                    winCounter = 1;
                }
            }
        }
    }
}
