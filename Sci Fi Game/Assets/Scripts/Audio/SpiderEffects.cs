﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class SpiderEffects : MonoBehaviour {
    
    public SpiderController spider;

    [Header( "Movement Noises" )]
    [Header( "Walk" )]
    public AudioSource WalkSource;
    public AudioClip[] walkSounds;
    [Header( "Run" )]
    public AudioSource RunSource;
    public AudioClip[] runSounds;

    [Header( "Sound Effects" )]
    [Header( "Idle / Patrol" )]
    public AudioSource IPSource;
    public AudioClip[] idlePatrolSounds;
    [Header( "Chase" )]
    public AudioSource ChaseSource;
    public AudioClip[] chaseSounds;
    [Header( "Look" )]
    public AudioSource LookSource;
    public AudioClip[] lookSounds;
    [Header( "Attack" )]
    public AudioSource AttackSource;
    public AudioClip[] attackSounds;

    private float counter;
    private float waitTimer = 5;
    private int h = 0;

    void Update( )
    {
        if( spider.idled == true )
        {
            if( h == 0 )
                WaitTimer( );
            h = 1;
            
            counter += Time.time;
            if( counter >= waitTimer )
                PlayIdlePatrolSound( );
            
        }
        else if( spider.walked == true )
        {
            if( h == 0 )
                WalkSource.PlayOneShot( walkSounds[0] );
            h = 1;

            counter += Time.deltaTime;
            if( counter >= waitTimer )
                PlayIdlePatrolSound( );
        }
        else if( spider.chased == true )
        {
            Debug.Log( "Play CHASING sound" );
            if( h == 0 )
                RunSource.PlayOneShot( runSounds[0] );
            h = 1;

            counter += Time.deltaTime;
            if( counter >= waitTimer )
                PlayChaseSound( );
        }

        if( spider.walked == false )
        {
            WalkSource.Stop( );
        }
        if( spider.chased == false )
        {
            RunSource.Stop( );
        }
    }

    void WaitTimer( )
    {
        waitTimer = Random.Range( 6f, 12f );
    }

    void PlayIdlePatrolSound( )
    {
        int n = Random.Range( 0, idlePatrolSounds.Length );
        IPSource.clip = idlePatrolSounds[n];
        IPSource.PlayOneShot( IPSource.clip );

        idlePatrolSounds[n] = idlePatrolSounds[0];
        idlePatrolSounds[0] = IPSource.clip;

        counter = 0;
        h = 0;
    }

    void PlayChaseSound()
    {
        int n = Random.Range( 0, chaseSounds.Length );
        RunSource.clip = chaseSounds[n];
        RunSource.PlayOneShot( RunSource.clip );

        chaseSounds[n] = chaseSounds[0];
        chaseSounds[0] = RunSource.clip;

        counter = 0;
        h = 0;
    }

}
