﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class SoundEffects : MonoBehaviour {

    private AudioSource effect;

    void Awake( )
    {
        effect = GetComponent<AudioSource>( );
    }

	void OnTriggerEnter( Collider other )
    {
        if( other.tag == "Player" )
        {
            effect.Play( );
        }
    }

    void OnTriggerExit( Collider other )
    {
        if( other.tag == "Player" )
        {
            effect.Stop( );
        }
    }
}
