﻿using UnityEngine;
using System.Collections;

public class PlayerEffects : MonoBehaviour {

    public PlayerController player;

    [Header( "Movement Noises" )]
    [Header( "Foot Steps" )]
    public AudioSource Source;
    public AudioClip[] Sounds;

    private float counter;
    private float waitTimer = 5;
    private int h = 0;

    void Update( )
    {
        if( player.walking == true )
        {
            counter += Time.deltaTime;
            if( counter >= 0.65f )
            {
                PlayFootSteps( );
            }
        }
        if( player.run == true )
        {
            counter += Time.deltaTime;
            if( counter >= 0.35f )
            { 
                PlayFootSteps( );
            }
        }
    }

    void WaitTimer( )
    {
        waitTimer = Random.Range( 0f, 2f );
    }

    void PlayFootSteps( )
    {
        int n = Random.Range( 0, Sounds.Length );
        Source.clip = Sounds[n];
        Source.PlayOneShot( Source.clip );

        Sounds[n] = Sounds[0];
        Sounds[0] = Source.clip;

        counter = 0;
    }
}
