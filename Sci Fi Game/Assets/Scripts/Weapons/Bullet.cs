﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour 
{
	private Rigidbody rb;

	void Awake( )
	{
		rb = GetComponent<Rigidbody>( );
	}

	public void Shoot( Vector3 shootDir, float speed )
	{
		rb.AddForce( shootDir * speed );
	}

	void OnTriggerEnter( Collider other )
	{
		if ( other.tag == "Spider" )
		{
			SpiderController spider = other.GetComponent<SpiderController>( );
			spider.SetStateToStun( true );
		}
		else if ( other.tag == "Centipede" )
		{
			CentipedeController centipede = other.GetComponent<CentipedeController>( );
			centipede.SetStateToStunned( );
		}

		Destroy( this.gameObject );
	}
}
