﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour 
{
	public delegate void Shot( int ammoCount );
	public Shot OnShoot;

	public Transform shotSpot;
	public GameObject bulletPrefab;

	public int ammo = 10;

	public float shootForce = 400f;

	public void Shoot( Vector3 shootDir )
	{
		if ( ammo <= 0 )
		{
			print( "Out of ammo!" );
			return;
		}
		else ammo--;

		if ( OnShoot != null )
			OnShoot( ammo );

		/*------------------------------------*/

		GameObject go = GameObject.Instantiate( bulletPrefab, shotSpot.position, shotSpot.rotation ) as GameObject;
		Bullet b = go.GetComponent<Bullet>( );

		b.Shoot( shootDir, shootForce );
	}
}
