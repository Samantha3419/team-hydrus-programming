﻿using UnityEngine;
using System.Collections;

public class SpiderChase : IState 
{
	private IState nextState = null;
	private SpiderController spider;

	public SpiderChase( SpiderController spider )
	{
		this.spider = spider;
		spider.GetNavAgent().ResetPath( );
        
        Debug.Log( "Spider Chase: " + spider.chased );
    }

	public void Update( )
    {
		spider.chased = true;
        spider.spiderAnim.SetBool("chase", true);

        bool sawPlayer = spider.visionSensor.SawThis( spider.GetPlayer() );

		Vector3 playerPos = FindPlayerPos( );
		playerPos.y = spider.transform.position.y;

		Vector3 dirToPlayer = ( playerPos - spider.transform.position ).normalized;

		spider.motor.LookTowards( dirToPlayer, spider.rotationSpeed );
		spider.motor.MoveInDirection( spider.transform.forward, spider.chaseSpeed );

		if( !sawPlayer && !spider.PlayerHiding() )
		{
			spider.spiderAnim.SetBool( "chase", false );
            nextState = new SpiderInspect( spider );
        }

		if( spider.InAttackRange() )
        {
			spider.spiderAnim.SetBool( "chase", false );
            nextState = new SpiderAttack( spider );
        }
	}

	Vector3 FindPlayerPos( )
	{
		Vector3 playerPos = Vector3.zero;

		if( spider.PlayerHiding() )
			playerPos = spider.PlayerHidingPos( );
		else
			playerPos = spider.GetPlayer().transform.position;

		return playerPos;
	}

	public IState GetNextState( )
	{
		return nextState;
	}
}
