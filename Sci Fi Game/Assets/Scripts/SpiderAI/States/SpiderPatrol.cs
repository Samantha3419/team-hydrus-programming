﻿using UnityEngine;
using System.Collections;

public class SpiderPatrol : IState 
{
	private IState nextState = null;
	private SpiderController spider;

	public SpiderPatrol( SpiderController spider )
	{
		this.spider = spider;

        Vector3 randomNode = GetRandomPatrolNode().position;

        this.spider.GetNavAgent().ResetPath( );
        this.spider.GetNavAgent().speed = spider.defaultSpeed;
		this.spider.GetNavAgent().SetDestination( randomNode );

        spider.walked = true;
        spider.chased = false;
	}

	public void Update( )
	{
        spider.spiderAnim.SetBool("patrol", true);

		bool sawPlayer = spider.visionSensor.SawThis( spider.GetPlayer() );
		bool heardPlayer = spider.noiseSensor.HeardNoiseFrom( spider.GetPlayer(), spider.CheckPlayerNoise() );

		if ( !spider.PlayerHiding() && sawPlayer )
        {
			Debug.Log( "WORKING" );
            spider.spiderAnim.SetBool("patrol", false);
            spider.walked = false;
            nextState = new SpiderChase( spider );
        }
		else if ( heardPlayer )
        {
            spider.spiderAnim.SetBool("patrol", false);
            spider.walked = false;
            nextState = new SpiderInspect( spider );
        }

		bool hasPath = spider.GetNavAgent().hasPath;
		if ( hasPath && spider.GetNavAgent().remainingDistance <= spider.GetNavAgent().stoppingDistance )
		{
			ChooseIdleOrPatrol( ); // A 50-50 chance for either one right now.
		}
	}

	public IState GetNextState( )
	{
		return nextState;
	}

	public Transform GetRandomPatrolNode( )
	{
        Vector3 currentNode = spider.transform.position;

		int size = spider.patrolPath.Count;
		int i = Random.Range( 0, size );

        if ( DistanceSquared(currentNode, spider.patrolPath[i].position) < 1f )
        {
            i = Mathf.Clamp( ++i, 0, spider.patrolPath.Count - 1 );
        }        

		return spider.patrolPath[i];
	}

	void ChooseIdleOrPatrol( )
	{
		// This will only return an odd or even number
		int rand = Random.Range( 1, 3 );
		switch ( rand )
		{
		case 1:
            spider.spiderAnim.SetBool("patrol", false);
            spider.walked = false;
            nextState = new SpiderIdle( spider );
            break;
		case 2:
			Vector3 randomNode = GetRandomPatrolNode().position;
			this.spider.GetNavAgent().SetDestination( randomNode );
			break;
		}
	}

    float DistanceSquared( Vector3 lhs, Vector3 rhs )
    {
        Vector3 temp = lhs - rhs;

        float x = temp.x * temp.x;
        float y = temp.y * temp.y;
        float z = temp.z * temp.z;

        return x + y + z;
    }



















}
