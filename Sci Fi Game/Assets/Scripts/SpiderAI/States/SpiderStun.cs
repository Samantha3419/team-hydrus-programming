﻿using UnityEngine;
using System.Collections;

public class SpiderStun : IState
{
	private SpiderController spider;
	private IState nextState = null;

	private float timer = 0;

	public SpiderStun( SpiderController spider )
	{
		this.spider = spider;
		spider.stunned = false;

		spider.motor.StopMoving( );
	}

	public void Update( )
	{
		timer += Time.deltaTime;

		if ( timer >= spider.stunTime )
		{
			nextState = new SpiderChase( spider );
		}
	}

	public IState GetNextState( )
	{
		return nextState;
	}
}
