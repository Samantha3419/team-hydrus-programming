﻿using UnityEngine;
using System.Collections;

public class SpiderInspect : IState 
{
	private IState nextState = null;
	private SpiderController spider;

	private float inspectTimer = 0;
	private float waitTime = 0;

	public SpiderInspect( SpiderController spider )
	{
		this.spider = spider;

		this.spider.motor.StopMoving( );
        this.spider.GetNavAgent().speed = spider.chaseSpeed;

		GameObject player = this.spider.GetPlayer();
		Vector3 playerPos = player.transform.position;
		playerPos += player.GetComponent<CharacterController>().velocity / 2f;
		
		this.spider.GetNavAgent().SetDestination( playerPos );

		waitTime = Random.Range( spider.minInspectionTime, spider.maxInspectionTime );
	}

	public void Update( )
    {
        spider.spiderAnim.SetBool("look", true);

        bool sawPlayer = spider.visionSensor.SawThis( spider.GetPlayer() );
		bool heardPlayer = spider.noiseSensor.HeardNoiseFrom( spider.GetPlayer(), spider.CheckPlayerNoise() );

        if ( !spider.PlayerHiding() && sawPlayer )
        {
            spider.spiderAnim.SetBool("look", false);
            nextState = new SpiderChase( spider );
        }
		else if ( heardPlayer )
        {
            spider.spiderAnim.SetBool("look", false);
            nextState = new SpiderInspect( spider );
        }

		bool hasPath = spider.GetNavAgent().hasPath;
		if ( hasPath && spider.GetNavAgent().remainingDistance < spider.GetNavAgent().stoppingDistance )
		{
			spider.GetNavAgent().Stop( );
		}

		inspectTimer += Time.deltaTime;
		if ( inspectTimer >= waitTime && !sawPlayer )
        {
            spider.spiderAnim.SetBool("look", false);
            nextState = new SpiderPatrol( spider );
		}
	}

	public IState GetNextState( )
	{
		return nextState;
	}













}
