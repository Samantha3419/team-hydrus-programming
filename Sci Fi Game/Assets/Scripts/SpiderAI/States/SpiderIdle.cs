﻿using UnityEngine;
using System.Collections;

public class SpiderIdle : IState 
{
	private IState nextState = null;
	private SpiderController spider;

	private float idleTimer = 0;
	private float waitTime = 0;

	public SpiderIdle( SpiderController spider )
	{
		this.spider = spider;

		spider.motor.StopMoving( );
		spider.GetNavAgent().ResetPath( );

		waitTime = Random.Range( spider.minIdleTime, spider.maxIdleTime );
        //Debug.Log( "Idle wait time: " + waitTime );

        spider.idled = true;
	}

	public void Update( )
	{
		spider.spiderAnim.SetBool("idle", true);
        bool sawPlayer = spider.visionSensor.SawThis( spider.GetPlayer() );
		bool heardPlayer = spider.noiseSensor.HeardNoiseFrom( spider.GetPlayer(), spider.CheckPlayerNoise() );

		if ( !spider.PlayerHiding() && sawPlayer )
		{
			spider.spiderAnim.SetBool("idle", false);
            spider.idled = false;
            nextState = new SpiderChase( spider );
		}
		else if ( heardPlayer )
		{
			spider.spiderAnim.SetBool("idle", false);
            spider.idled = false;
            nextState = new SpiderInspect( spider );
		}

		idleTimer += Time.deltaTime;
		if ( idleTimer >= waitTime )
		{
			spider.spiderAnim.SetBool("idle", false);
            spider.idled = false;
            nextState = new SpiderPatrol( spider );
		}
	}

	public IState GetNextState( )
	{
		return nextState;
	}


















}
