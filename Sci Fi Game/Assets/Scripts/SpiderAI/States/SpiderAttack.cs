﻿using UnityEngine;
using System.Collections;

public class SpiderAttack : IState 
{	
	private IState nextState = null;
	private SpiderController spider;

	public SpiderAttack( SpiderController spider )
	{
		this.spider = spider;
		this.spider.motor.StopMoving( );
    }
	
	public void Update( )
    {
        spider.spiderAnim.SetBool( "attack", true );
        Debug.Log( "Atacking..." );

        Vector3 dirToPlayer = ( spider.GetPlayer().transform.position - spider.transform.position ).normalized;
		spider.motor.LookTowards( dirToPlayer, spider.rotationSpeed );

        if( spider.missed )
        {
            Debug.Log( "MISSED.." );
            bool sawPlayer = spider.visionSensor.SawThis(spider.GetPlayer());

            if( sawPlayer )
            {
                spider.spiderAnim.SetBool( "attack", false );
                spider.missed = false;
                nextState = new SpiderChase( spider );
            }
        }
    }
	
	public IState GetNextState( )
	{
		return nextState;
	}
}
