﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpiderController : MonoBehaviour 
{
	private GameObject player;
	private PlayerController playerController;
	private NavMeshAgent navAgent;

	public MoveActuator motor;
	public VisionSensor visionSensor;
	public NoiseSensor noiseSensor;

    public Animator spiderAnim;

	private IState state;

	public List<Transform> patrolPath = new List<Transform>( );

	[Header( "Vision Info" )]
	public float visionRange = 15f;
	public float visionFOV = 45f;
	public float rotationSpeed = 30f;
	public LayerMask layerMask;

	[Header( "Hearing Info" )]
	public float hearingRange = 3f;

	[Header( "State Info" )]
    public float defaultSpeed = 3.5f;
	public float chaseSpeed = 5f;
	public float attackRange = 5f;
	public float stunTime = 1f;
	public float minIdleTime = 1f;
	public float maxIdleTime = 3f;
	public float minInspectionTime = 3f;
	public float maxInspectionTime = 5f;

    public bool idled = false;
    public bool walked = false;
    public bool attacked = false;
    public bool missed = false;
    public bool heared = false;
	public bool chased = false;
	public bool stunned = false;

	void Awake( )
	{
		player = GameObject.FindGameObjectWithTag( "Player" );
		playerController = player.GetComponent<PlayerController>( );
		navAgent = GetComponent<NavMeshAgent>( );

        int size = patrolPath.Count;
        int i = Random.Range(0, size);
        this.gameObject.transform.position = patrolPath[i].position;

		state = new SpiderPatrol( this );

		motor = new MoveActuator( this.gameObject );
		visionSensor = new VisionSensor( this.gameObject, visionRange, visionFOV );
		visionSensor.SetLayerMask( layerMask );
		noiseSensor = new NoiseSensor( this.gameObject, hearingRange );

        spiderAnim = GetComponent<Animator>( );
    }
    
	void Update( )
    {
        state.Update( );

		IState nextState = state.GetNextState( );
		if ( stunned )
			nextState = new SpiderStun( this );

		if ( nextState != null )
		{
			state = nextState;
			print( state );
		}
	}

	public void SetStateToStun( bool value )
	{
		stunned = value;
	}

	public GameObject GetPlayer( )
	{
		return player;
	}

	public NavMeshAgent GetNavAgent( )
	{
		return navAgent;
	}

	public bool InAttackRange( )
	{
		Vector3 playerPos = player.transform.position;
		if ( PlayerHiding() )
			playerPos = PlayerHidingPos( );

		if ( Vector3.Distance(transform.position, playerPos) <= attackRange )
			return true;

		return false;
	}

	void OnDrawGizmos( )
	{
		Gizmos.color = Color.magenta;
		Gizmos.DrawWireSphere( transform.position, hearingRange );
	}

	public float CheckPlayerNoise( )
	{
		return playerController.NoiseRadius( );
	}

	public bool PlayerHiding( )
	{
		return playerController.IsHiding( );
	}

	public Vector3 PlayerHidingPos( )
	{
		return playerController.HidingPosition( );
	}

    public void SpiderRestart( )
    {
        //Give the spider a new position
        int size = patrolPath.Count;
        int i = Random.Range( 0, size );
        transform.position = patrolPath[i].position;

        attacked = false;
        spiderAnim.SetBool( "attack", false );
        spiderAnim.SetBool( "chase", false );
        spiderAnim.SetBool( "patrol", false );
        spiderAnim.SetBool( "look", false );

        state = new SpiderPatrol( this );
    }

    void Attack( )
    {
        Debug.DrawRay( transform.position, transform.forward * 5, Color.green );
        RaycastHit hit;
        if( Physics.Raycast(transform.position, transform.forward * 5, out hit, 5.0f) )
        {
            PlayerController player = hit.collider.gameObject.GetComponent<PlayerController>( );

            if( player != null )
                attacked = true;
        }
    }

    void Missed( )
    {
        missed = true;
    }






























}
