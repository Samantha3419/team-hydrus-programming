﻿using UnityEngine;
using System.Collections;

public interface IState 
{
	void Update( );
	IState GetNextState( );
}
