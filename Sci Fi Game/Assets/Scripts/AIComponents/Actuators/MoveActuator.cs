﻿using UnityEngine;
using System.Collections;

public class MoveActuator 
{
	private GameObject myself;
	private Rigidbody rb;

	public MoveActuator( GameObject me )
	{
		myself = me;
		rb = myself.GetComponent<Rigidbody>( );

		if ( !rb )
		{
			Debug.Log( myself.name + " doesn't have a RigidBody. Please attach one." );
		}
	}

	public void LookTowards( Vector3 dir, float rotationSpeed )
	{
		myself.transform.forward = Vector3.RotateTowards( myself.transform.forward,
		                                             	  dir,
		                                             	  rotationSpeed * Time.deltaTime,
		                                             	  0 );
	}

	public void MoveInDirection( Vector3 dir, float moveSpeed )
	{
		rb.velocity = dir * moveSpeed;
	}

	public void StopMoving( )
	{
		rb.velocity = Vector3.zero;
	}

	public void Rotate( Vector3 axis, float rotationSpeed )
	{
		myself.transform.Rotate( axis, rotationSpeed );
	}













}
