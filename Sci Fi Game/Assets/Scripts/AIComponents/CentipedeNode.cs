﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CentipedeNode : MonoBehaviour 
{
	public bool isVentNode = false;
	public bool isExitNode = false;

	public List<CentipedeNode> neighbors = new List<CentipedeNode>( );

	static public List<CentipedeNode> ventNodes = new List<CentipedeNode>( );
	static public List<CentipedeNode> exitNodes = new List<CentipedeNode>( );

	public float gCost = 0;
	public float hCost = 0;
	public float fCost {
		get {
			return gCost + hCost;
		}
	}

	public CentipedeNode parent = null;

	void Awake( )
	{
		if ( isVentNode )
			ventNodes.Add( this );
		if ( isExitNode )
			exitNodes.Add( this );
	}

	public Vector3 Position( )
	{
		return this.transform.position;
	}
}
