﻿using UnityEngine;
using System.Collections;

public class Node : MonoBehaviour 
{
	public Node next = null;
	public Node prev = null;

	public Vector3 Position( )
	{
		return transform.position;
	}
}
