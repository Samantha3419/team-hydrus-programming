﻿using UnityEngine;
using System.Collections;

public class VisionSensor
{
	private GameObject myself;

	private float range = 5f;
	private float FOV = 30f;

	private LayerMask layerMask;

	public VisionSensor( GameObject me, float range, float FOV )
	{
		myself = me;
		this.range = range;
		this.FOV = FOV;
	}

	public bool SawThis( GameObject other )
	{
		Vector3 myPos = myself.transform.position;
		Vector3 otherPos = other.transform.position;

		DebugDrawFOV( );

		if ( Vector3.Distance(myPos, otherPos) > range )
			return false;

		Vector3 dirToOther = ( otherPos - myPos ).normalized;
		float angle = Vector3.Angle( myself.transform.forward, dirToOther );

		if ( angle <= FOV )
		{
			Debug.DrawRay( myPos, dirToOther * range );

			RaycastHit hit;
			if ( Physics.Raycast(myPos, dirToOther, out hit, range, layerMask) )
			{
				if ( hit.transform.tag == other.tag )
					return true;
			}
		}

		return false;
	}

	void DebugDrawFOV( )
	{
		float radFOV = FOV * Mathf.Deg2Rad;
		Vector3 rightDir = Mathf.Cos(radFOV) * myself.transform.forward + Mathf.Sin(radFOV) * myself.transform.right;
		Vector3 leftDir = Mathf.Cos(radFOV) * myself.transform.forward - Mathf.Sin(radFOV) * myself.transform.right;
		Debug.DrawRay( myself.transform.position, rightDir * range );
		Debug.DrawRay( myself.transform.position, leftDir * range );
	}

	public void SetLayerMask( LayerMask mask )
	{
		layerMask = mask;
	}

	public void SetFOV( float FOV )
	{
		this.FOV = FOV;
	}
	public void SetRange( float range )
	{
		this.range = range;
	}
}
