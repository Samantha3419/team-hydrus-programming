﻿using UnityEngine;
using System.Collections;

public class NoiseSensor 
{
	private GameObject myself;

	private float hearingRadius = 5f;

	public NoiseSensor( GameObject me, float hearingRadius )
	{
		myself = me;
		this.hearingRadius = hearingRadius;
	}

	public bool HeardNoiseFrom( GameObject other, float otherNoiseRadius )
	{
		if ( otherNoiseRadius <= 0 )
			return false;

		float distance = Vector3.Distance( myself.transform.position, other.transform.position );

		if ( distance <= otherNoiseRadius + hearingRadius )
		{
			return true;
		}

		return false;
	}

	public void SetHearingRadius( float hearingRadius )
	{
		this.hearingRadius = hearingRadius;
	}
}
