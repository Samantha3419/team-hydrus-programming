﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor( typeof(SpiderController) )]
public class SpiderController_Editor :  Editor
{
	public override void OnInspectorGUI( )
	{
		DrawDefaultInspector( );
	}

	void OnSceneGUI( )
	{
		SpiderController spider = target as SpiderController;

		foreach ( Transform t in spider.patrolPath )
		{
			t.position = Handles.PositionHandle( t.position, t.rotation );
			Handles.Label( t.position, t.name );
			
			EditorUtility.SetDirty( spider );
		}
	}
}
