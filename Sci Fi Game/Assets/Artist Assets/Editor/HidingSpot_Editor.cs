﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor( typeof(Hideable), true )]
public class HidingSpot_Editor : Editor
{
	public override void OnInspectorGUI( )
	{
		Hideable hideObj = (Hideable)target;
		DrawDefaultInspector( );

		if ( GUILayout.Button("Add Exit Spot") )
		{
			Vector3 exitSpot = hideObj.transform.position + hideObj.transform.right* 1.5f;
			hideObj.exitSpots.Add( exitSpot );

			EditorUtility.SetDirty( hideObj );
		}
	}

	public void OnSceneGUI( )
	{
		Hideable hideObj = (Hideable)target;

		for ( int i = 0; i < hideObj.exitSpots.Count; i++ )
		{
			hideObj.exitSpots[i] = Handles.PositionHandle( hideObj.exitSpots[i], Quaternion.identity );
			Handles.Label( hideObj.exitSpots[i], "ExitSpot_" + i );

			EditorUtility.SetDirty( hideObj );
		}
	}
}
