﻿using UnityEngine;
using System.Collections;

public class Animate : MonoBehaviour 
{
	private float spiderSpeed;
	private Animator anim;

	// Use this for initialization
	void Start () 
	{
		anim = gameObject.GetComponentInChildren<Animator> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		spiderSpeed = gameObject.GetComponent<Rigidbody> ().velocity.magnitude;
		Debug.Log (spiderSpeed);
		anim.SetFloat ("speed", spiderSpeed);
		
	}
}
