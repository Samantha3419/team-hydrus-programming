﻿using UnityEngine;
using System.Collections;

public class Light_Flicker : MonoBehaviour 
{
	public Material flicker_mat;

	public GameObject lightGroup;

	private bool opp = true;
	
	// Use this for initialization
	void Awake () 
	{
		StartCoroutine ("Flicker");
	}

	IEnumerator Flicker()
	{
		Color baseColor = Color.white;
		Color darkColor = baseColor * 0.1f;
		Color lightColor = baseColor * 0.6f;

		while (true) 
		{
			yield return new WaitForSeconds (1f);
			lightGroup.SetActive(opp);
			opp = !opp;

			if(opp == false)
				flicker_mat.SetColor("_EmissionColor", lightColor);

			if(opp == true)
				flicker_mat.SetColor("_EmissionColor", darkColor);
			//Color finalColor = baseColor * Random.Range (0.3f, 0.8f);
			//flicker_mat.SetColor ("_EmissionColor", finalColor);
		}
	}
}
